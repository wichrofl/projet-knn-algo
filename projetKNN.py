#!/usr/bin/env python
# coding: utf-8
#utilisation sous jupyterhub

import json
from typing import Callable
import math
import glob
import os


class TextVect:

    """ Méthodes suivantes : 
    - méthode sim_cosinus()
    - méthode doc2vec(filename)
    - méthode filter(stopwords,hapax)
    - méthode tf_idf(docs) (non obligatoire car peut générer des prblms) """

    #nom du texte, constructeur
    def __init__(self, nom, texte, stoplist=set(), non_hapax=True):
        self._nom=nom
        self._vector={}
        
        tokens=tokenize(texte)
        
        for tok in tokens:
            if tok in self._vector:
                self._vector[tok]+=1
            else:
                self._vector[tok]=1
                
        self.filtrage(stoplist, non_hapax)

    # fonction filtrage (c) Xuetao repris du collab CAH, modifié ensuite
    def filtrage(self, stoplist:set, non_hapax):
        """
        A partir d'une liste de documents (objets avec deux propriétés 'label' et 'vect')
        on élimine tous les vocables appartenant à la stoplist.
        Input :
          arg1 : set - l'ensemble des stopwords
          arg2 : list(doc) - un doc est un dict contenant deux clés : 'label' et 'vect'
                doc : { 'label':str, 'vect':dict }
          arg3 : bool - indique si on veut éliminer les hapax (True) ou non (False)

        """
        tokens_filtre = {}
        # on parcourt chaque token dans les clés de dict tokens
        for token in self._vector.keys():
        # selon le choix de l'utilisateur, si on veut éliminer les hapax, on exécute les codes ci-desous
            if token.lower() not in stoplist and (not non_hapax or self._vector[token]>1):
                tokens_filtre[token]=self._vector[token]

        self._vector=tokens_filtre 
        
    #pour rendre jolie la sortie :-)
    def __repr__(self):
        return str({"nom":self._nom, "valeur":self._vector})

    
#code qui a été pris dans le collab CAH du cours
"""
Cette fonction récupère deux vecteurs sous forme de hashage 
et renvoie leur produit scalaire
Input :
    arg1 : vector1 - hash
    arg2 : vector2 - hash
Output :
    valeur de retour : un produit scalaire - float
"""

def scalaire(vector1,vector2):
    liste_scalaire=[]
    for key in vector1:
        if key in vector2:
            liste_scalaire.append(vector1[key]*vector2[key])
    produit_scalaire=sum(liste_scalaire)
    return produit_scalaire

"""
Cette fonction récupère un vecteur sous forme de hashage 
et renvoie sa norme
Input :
    arg1 : vector - hash
Output :
    valeur de retour : une norme - float
"""

def norme(vector):
    norme_carre=0
    for key in vector:
        # print(key,"=>",vector[key])
        norme_carre+=vector[key]*vector[key]
    norme=math.sqrt(norme_carre)
    return norme

"""
Cette fonction récupère deux vecteurs sous forme de hashage, 
et renvoie leur cosinus
en appelant les fonctions Scalaire et Norme
Input :
    arg1 : vector1 - hash
    arg2 : vector2 - hash
Output :
    valeur de retour : un cosinus - float
"""

def sim_cosinus(vector1,vector2):
    norme1=norme(vector1)
    norme2=norme(vector2)
    scal=scalaire(vector1,vector2)
    cosinus=(scal/(norme1*norme2))
    return cosinus

def dist_cosinus(vector1,vector2):
    return 1-sim_cosinus(vector1,vector2)  

class KNNClasses:

    """ KNNClasses contenant les propriétés suivantes :
    - description : une string contenant la description de la classification
    - data : une liste de classes, qui correspond à la structure JSON label/vectors
    
    Ainsi que les méthodes suivantes :
    - méthode add_class(label:str,vectors:list) : pour ajouter une nouvelle classe
    - méthode add_vector(label:str,vector:vect) : pour ajouter un vecteur à une classe définie par un label 
    - méthode del_class(label:str) : pour supprimer la classe correspondant à label
    - méthode save_as_json(filename:str) : pour enregistrer les données d'une classe au format json
    - méthode load_as_json(filename:str) : pour charger les données depuis une classe au format json
    - méthode classify(vector:vect,k:int,sim_func:function) : qui renvoie la liste des classes candidates
    pour le vecteur, comme liste triée de paires [label:str,sim:float]. """   
    
    #fonction constructeur, instanstiation 
    def __init__(self,description):
        self._description=description
        self._data=[]
    
    #fonction qui prends le nom de la classe et la liste des vecteurs 
    def add_class(self,label:str,vectors:list):
        """ pour ajouter une nouvelle classe """
        content={"label":label,"vectors":vectors}
        self._data.append(content)
    
    #fonction qui prends le nom de la classe et la liste des vecteurs
    def add_vector(self,label:str,vector:dict):
        """ pour ajouter un vecteur à une classe définie par un label """    
        for classe in self._data:
            if label==classe["label"]:
                classe["vectors"].append(vector)
                return
            
    #fonction qui prends le nom de la classe
    def del_class(self,label:str):
        """ pour supprimer la classe correspondant à label """
        for i,classe in enumerate(self._data):
            print(label)
            print(classe["label"])
            if label==classe["label"]:
                del self._data[i]
                return  
    
    def save_as_json(self,filename:str):
        """ enregistrer données d'une classe au format json """
        f=open(filename,mode="w",encoding="utf-8")
        
        donnees={"description":self._description, "data":[]}
        for classe in self._data: 
            
            vekts=[]
            
            for vekt in classe["vectors"]:
                
                #permet de récupérer sous forme de dictionnaire pour sérialiser
                vekts.append(vekt.__dict__)
            
            donnees["data"].append({"label":classe["label"],"vectors":vekts})
            
        
        json.dump(donnees, f, indent="\t", ensure_ascii=False)     
        f.close()
    
    @classmethod
    def load_as_json(cls,filename:str):
        """ pour charger les données depuis une classe au format json """
        f=open(filename,mode="r",encoding="utf-8")
        donnees=json.load(f)  
        
        knn=cls(donnees["description"])
        
        #on prends pr chaque classe les dictionnaires pour les convertir en TextVect
        for classe in donnees["data"]:
            
            liste_vect=[]
            for vector in classe["vectors"]:
                texte=TextVect(vector["_nom"],"")
                texte._vector=vector["_vector"]
                liste_vect.append(texte)
            knn.add_class(classe["label"],liste_vect)

        f.close()
        return knn
        
    def classify(self,vector:TextVect,k:int,sim_func:Callable):
        """ qui renvoie la liste des classes candidates pour le vecteur """
        """ comparer vector à vecteur dans classes """
       
        donnees=[]
        
        for classe in self._data:
            
            for vect in classe["vectors"]:
                donnees.append((classe["label"], sim_func(vector._vector,vect._vector)))
                
        #trie ordre décroissant
        donnees.sort(key=lambda x: x[1], reverse=True)
        donnees=donnees[:k]
        
        #trie en fonction de la classe
        donnees.sort(key=lambda x: x[0])
        
        #pointeur qui montre si on est toujours dans la classe ou non
        pointeur=""
        #liste pour stocker résultats
        sortie=[]
        #initialisation du buffer
        buffer=[]
        
        #cette boucle va nous permettre de prendre toutes les données de chaque classes 
        # en faisant leurs moyennes grâce à notre pointeur qui détecte à chaque fois que l'on sort d'une classe
        #(très heureuse de mon idée, en espèrant qu'elle va bien marcher)
        for element in donnees:
            if pointeur!=element[0]:
                
                if pointeur!="":
                    #moyenne
                    sortie.append((pointeur,sum(buffer)/len(buffer), len(buffer)))
                    buffer=[]
                
                pointeur=element[0]
            
            buffer.append(element[1])
        #permet que la dernière classe se fasse bien    
        sortie.append((pointeur,sum(buffer)/len(buffer), len(buffer)))
        
        return sortie     

#code repris dans un exemple début d'année, en dehors de la classe
def tokenize(text:str)->list:
    tokens=[]

    word=""
    punctuations=["'",",","?",";",".",":","!","(",")","\"","[","]"]
    for (i,car) in enumerate(text+" "):
        # si on rencontre un séparateur et 
        # si le mot en construction n'est pas vide -> on produit la sortie et on incrémente
        # cas 1 : on rencontre un séparateur
        if (car==" " or car in punctuations):
            # cas 1.1 : si le précédent car n'était pas un séparateur
            if (word!="") :
                # cas 1.1.1
                if (car=='\'') :
                    word=word+car

                # on a un mot complet
                tokens.append(word)
                if car!=" " and car!="'":
                    tokens.append(car)
                # on vide le mot courant
                word=""
            # cas 1.2 : on a un séparateur après un séparateur
            else:
                if car!=" ":
                    tokens.append(car)
        # cas 2 : on rencontre un car. de mots
        else:
            # on a un caractère de mot, qu'on enregistre dans word
            word=word+car # word+=car
    return (tokens)
    
#récupérer un ensemble de fichiers, découverte de glob grâce à Jérémy
def allfichiers(chemin, stoplist=set(), non_hapax=True):
    fichiers=glob.glob(chemin + "/*")
    print(fichiers)
    vekts=[]
    
    for fichier in fichiers:
        with open(fichier,mode="r",encoding="utf-8") as f:
            vekts.append(TextVect(os.path.basename(fichier),f.read(), stoplist, non_hapax))
    return vekts
    
# fonction prise du CAH pour permettre l'utilisation de la stoplist, la générer 
def read_dict(stoplist_filename):
    """
    Lecture d'une stoplist à partir d'un fichier
    Input : 
      arg1 : str - nom du fichier à lire. Un mot par ligne.
    Output :
      valeur de retour : set(str) - ensemble des stopwords
    """
    # on ouvre, lit et après ferme le fichier
    dict_file = open(stoplist_filename, "r", encoding="utf-8")
    dict_content = dict_file.read()
    dict_file.close()
    # on sépare le dict_content(string) avec la saut de ligne et renvoie une liste
    stoplist = set(dict_content.split("\n"))
    return stoplist
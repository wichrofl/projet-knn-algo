# Projet KNN - algo

## Compte rendu

### Pour le premier test.
En tout premier, il faut créer une base de test : dans le *main* nous pouvons voir que nous avons **"animaux"** et **"nourritures"** puisque nous avons dans un autre dossier plusieurs textes qui se basent sur ses données. A
savoir que nos textes d'entrées seront enregistrés sous format .json. 

Par la suite nous devons utiliser la fonction `add_class` : celle-ci prends le nom de la classe en entrée (ici, "animaux" et "nourritures", toujours les mêmes). Nous spécifions grâce à la fonction `allfichiers` qu'il faut prendre
tous les fichiers du dossier où se trouve notre base de données (ici "Textes animaux" et "Textes nourritures", où se trouvent tous nos textes). 

La fonction `save_as_json` permettra ensuite d'enregistrer les données de nos classes.  

Ce premier test est indispensable pour créer notre fichier json avec nos bases de données, sans ça rien ne fonctionne.


### Pour le deuxième test.
Ici la fonction `load_as_json` permet de charger les données des classes tests pour vérifier qu'il n'y a eu aucune perte.

### Pour le troisième test.
Le set stoplist permet de supprimer les mots qui sont considérés comme inutile dans un texte pour de la comparaison : ponctuations, déterminants... 

## Améliorations auxquelles je pense :
- [ ] Prendre en compte le `tf_idf`.
- [ ] Utiliser d'autres fonctions de similarité comme `jaccard` par exemple, plus rapide que `cosinus`.
- [ ] Pour de gros corpus nous pourrions répartir les calculs dans plusieurs processus pour réduire le temps de traitement, ce qui se fait par exemple sur les pages internets.
- [ ] Implanter une validation croisée pour la précision et l'amélioration du code.
- [ ] Mieux documenter mes classes et fonctions.
- [ ] Rendu plus propre et lisible pour la visualisation du contenu.

## Pour finir sur une note rigolote 🍕
![Meme](/meme.gif "meme")
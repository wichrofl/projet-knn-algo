#relier les 2
from projetKNN import *

#on créer et on enregistre nos données
filename="fichier.json"

knn1=KNNClasses("Ceci est une description")

knn1.add_class("nourriture", allfichiers("Textes nourritures"))
knn1.add_class("animaux", allfichiers("Textes animaux"))

knn1.save_as_json(filename)

print(knn1.classify(TextVect("oui", "La pizza est un plat hautement supérieur à la raclette. Nous pouvons observer que la pizza est un plat fait en Italie tout comme le panini. La pizza è magnifico."), 3, sim_cosinus)) 

#on charge nos données, pas de perte
filename="fichier.json"

knn2=KNNClasses.load_as_json(filename)

print(knn2.classify(TextVect("oui", "La pizza est un plat hautement supérieur à la raclette. Nous pouvons observer que la pizza est un plat fait en Italie tout comme le panini. La pizza è magnifico."), 3, sim_cosinus)) 

#pour voir le contenu
knn3=KNNClasses.load_as_json(filename)
print("Avant\n",knn3._data)

knn3.del_class("animal")
print("Après\n",knn3._data)

#stoplist

stoplist=read_dict("stoplist.txt")

knn4=KNNClasses("Ceci est une description")

knn4.add_class("nourriture", allfichiers("Textes nourritures",stoplist=stoplist,non_hapax=True))
knn4.add_class("animaux", allfichiers("Textes animaux",stoplist=stoplist,non_hapax=True))

print(knn4.classify(TextVect("oui", "La pizza est un plat hautement supérieur à la raclette. Nous pouvons observer que la pizza est un plat fait en Italie tout comme le panini. La pizza è magnifico.", stoplist=stoplist,non_hapax=True), 3, sim_cosinus))
print("Finalité\n",knn4._data)